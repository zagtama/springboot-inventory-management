package com.service.inventorymanagement.services.impl;

import com.service.inventorymanagement.models.merchant.Merchant;
import com.service.inventorymanagement.services.MerchantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;

public class MerchantServiceImpl implements MerchantService {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public Page<Merchant> findAll(String name, String phone, String email, Pageable pageable) {
        Criteria criteria = Criteria.where("deleteStatus").is(false);

        if (name != null && !name.trim().equals("")) {
            criteria = criteria.and("name").regex(name.trim(), "i");
        }

        if (phone != null && !phone.trim().equals("")) {
            criteria = criteria.and("phone").regex(phone.trim(), "i");
        }

        if (email != null && !email.trim().equals("")) {
            criteria = criteria.and("email").regex(email.trim(), "i");
        }

        AggregationOperation match = Aggregation.match(criteria);
        AggregationOperation skip = Aggregation.skip((long) pageable.getPageNumber() * pageable.getPageSize());
        AggregationOperation limit = Aggregation.limit(pageable.getPageSize());
        AggregationOperation sort = Aggregation.sort(pageable.getSort());

        Aggregation aggregation = Aggregation.newAggregation(match, sort, skip, limit);

        AggregationResults<Merchant> aggregationResults = mongoTemplate.aggregate(aggregation, "merchant", Merchant.class);
        List<Merchant> listBank = aggregationResults.getMappedResults();

        long countResult = mongoTemplate.count(new Query(criteria), Merchant.class);

        return new PageImpl<>(listBank, pageable, countResult);
    }
}
