package com.service.inventorymanagement.services.impl;

import com.service.inventorymanagement.models.product.Product;
import com.service.inventorymanagement.services.ProductService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

public class ProductServiceImpl implements ProductService {

    @Autowired
    MongoTemplate mongoTemplate;

    public Page<Product> findAllByMerchantId(ObjectId merchantId, String name, Pageable pageable) {

        Criteria criteria = Criteria.where("deleteStatus").is(false)
                .and("merchantId").is(merchantId);

        if (name != null && !name.trim().equals("")) {
            criteria = criteria.and("name").regex(name.trim(), "i");
        }

        AggregationOperation match = Aggregation.match(criteria);
        AggregationOperation skip = Aggregation.skip((long) pageable.getPageNumber() * pageable.getPageSize());
        AggregationOperation limit = Aggregation.limit(pageable.getPageSize());
        AggregationOperation sort = Aggregation.sort(pageable.getSort());

        Aggregation aggregation = Aggregation.newAggregation(match, sort, skip, limit);

        AggregationResults<Product> aggregationResults = mongoTemplate.aggregate(aggregation, "product", Product.class);
        List<Product> listBank = aggregationResults.getMappedResults();

        long countResult = mongoTemplate.count(new Query(criteria), Product.class);

        return new PageImpl<>(listBank, pageable, countResult);
    }
}
