package com.service.inventorymanagement.services;

import com.service.inventorymanagement.models.merchant.Merchant;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MerchantService {

    public Page<Merchant> findAll(String name, String phone, String email, Pageable pageable);

}
