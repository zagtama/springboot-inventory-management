package com.service.inventorymanagement.services;

import com.service.inventorymanagement.models.merchant.Merchant;
import com.service.inventorymanagement.models.product.Product;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProductService {

    public Page<Product> findAllByMerchantId(ObjectId merchantId, String name, Pageable pageable);

}
