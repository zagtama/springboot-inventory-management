package com.service.inventorymanagement.enums;

public enum UserRoles {
    ROLE_OFFICE, ROLE_ADMIN, ROLE_USER
}
