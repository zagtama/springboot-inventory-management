package com.service.inventorymanagement.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.service.inventorymanagement.helpers.exceptions.BadRequestException;
import com.service.inventorymanagement.helpers.exceptions.NotFoundException;
import com.service.inventorymanagement.models.merchant.Merchant;
import com.service.inventorymanagement.models.merchant.MerchantRequest;
import com.service.inventorymanagement.repository.MerchantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Optional;


@RestController
@RequestMapping("/api/merchant")
public class MerchantController {

    @Autowired
    private MerchantRepository merchantRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @PostMapping("")
    public Merchant addMerchant(@RequestBody MerchantRequest merchantRequest) {
        Merchant merchantData;
        merchantData = merchantRepository.findByEmail(merchantRequest.getEmail().trim().toLowerCase());
        if (merchantData != null) {
            throw new BadRequestException("Email already registered in the system. Email: " + merchantRequest.getEmail().trim().toLowerCase());
        }
        merchantData = merchantRepository.findByPhone(merchantRequest.getPhone().trim());
        if (merchantData != null) {
            throw new BadRequestException("Phone already registered in the system. Phone: " + merchantRequest.getPhone().trim());
        }

        Merchant merchant = new Merchant();
        merchant.setCreateTime(LocalDateTime.now());
        merchant.setCreatorName("SYSTEM");
        merchant.setName(merchantRequest.getName());
        merchant.setEmail(merchantRequest.getEmail());
        merchant.setPhone(merchantRequest.getPhone());
        merchant.setDescription(merchantRequest.getDescription());

        merchantRepository.insert(merchant);

        return merchant;
    }

    @GetMapping("/all")
    public Page<Merchant> getAllMerchant(@RequestParam(required = false, value = "") String name,
                                         @RequestParam(required = false, value = "") String phone,
                                         @RequestParam(required = false, value = "") String email,
                                         Pageable pageable) {
        return merchantRepository.findAll(name, phone, email, pageable);
    }

    @GetMapping("/{merchantId}")
    public Merchant getMerchant(@PathVariable String merchantId) {
        Optional<Merchant> merchantOptional = merchantRepository.findById(merchantId);
        if (merchantOptional.isEmpty()) {
            throw new NotFoundException("Merchant data not found. ID: " + merchantId);
        }

        Merchant merchant = merchantOptional.get();
        if (merchant.isDeleteStatus()) {
            throw new NotFoundException("Merchant data not found. ID: " + merchantId);
        }

        return merchant;
    }

    @PutMapping("/{merchantId}")
    public Merchant editMerchant(@PathVariable String merchantId, @RequestBody MerchantRequest merchantRequest) {
        Merchant merchantData;
        merchantData = merchantRepository.findByEmail(merchantRequest.getEmail().trim().toLowerCase());
        if (merchantData != null) {
            if (!merchantData.getEmail().equals(merchantRequest.getEmail().trim().toLowerCase())) {
                throw new BadRequestException("Email already registered in the system. Email: " + merchantRequest.getEmail().trim().toLowerCase());
            }
        }
        merchantData = merchantRepository.findByPhone(merchantRequest.getPhone().trim());
        if (merchantData != null) {
            if (!merchantData.getPhone().equals(merchantRequest.getPhone().trim())) {
                throw new BadRequestException("Phone already registered in the system. Phone: " + merchantRequest.getPhone().trim());
            }
        }

        Optional<Merchant> merchantOptional = merchantRepository.findById(merchantId);
        if (merchantOptional.isEmpty()) {
            throw new NotFoundException("Merchant data not found. ID: " + merchantId);
        }

        Merchant merchant = merchantOptional.get();
        if (merchant.isDeleteStatus()) {
            throw new NotFoundException("Merchant data not found. ID: " + merchantId);
        }

        merchant.setUpdateTime(LocalDateTime.now());
        merchant.setUpdaterName("SYSTEM");
        merchant.setName(merchantRequest.getName());
        merchant.setEmail(merchantRequest.getEmail());
        merchant.setPhone(merchantRequest.getPhone());
        merchant.setDescription(merchantRequest.getDescription());

        merchantRepository.save(merchant);

        return merchant;
    }

    @DeleteMapping("/{merchantId}")
    public ObjectNode deleteMerchant(@PathVariable String merchantId) {
        Optional<Merchant> merchantOptional = merchantRepository.findById(merchantId);
        if (merchantOptional.isEmpty()) {
            throw new NotFoundException("Merchant data not found. ID: " + merchantId);
        }

        Merchant merchant = merchantOptional.get();
        if (merchant.isDeleteStatus()) {
            throw new NotFoundException("Merchant data not found. ID: " + merchantId);
        }

        merchant.setDeleteTime(LocalDateTime.now());
        merchant.setDeleterName("SYSTEM");
        merchant.setDeleteStatus(true);

        merchantRepository.save(merchant);

        ObjectNode response = objectMapper.createObjectNode();
        response.put("message", "Success delete merchant. ID: " + merchantId);
        return response;
    }
}
