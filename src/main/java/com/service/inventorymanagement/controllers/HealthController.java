package com.service.inventorymanagement.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/health")
public class HealthController {

    @Autowired
    private ObjectMapper objectMapper;

    @GetMapping
    public ObjectNode check() {
        ObjectNode jsonNodes = objectMapper.createObjectNode();
        jsonNodes.put("status", "OK");

        return jsonNodes;
    }
}
