package com.service.inventorymanagement.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.service.inventorymanagement.helpers.exceptions.BadRequestException;
import com.service.inventorymanagement.helpers.exceptions.NotFoundException;
import com.service.inventorymanagement.models.account.Account;
import com.service.inventorymanagement.models.account.AccountRequest;
import com.service.inventorymanagement.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Optional;


@RestController
@RequestMapping("/api/account")
public class AccountController {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private ObjectMapper objectMapper;


    @PostMapping("")
    public Account addAccount(@RequestBody AccountRequest accountRequest) {
        Account accountData;
        accountData = accountRepository.findByEmail(accountRequest.getEmail().trim().toLowerCase());
        if (accountData != null) {
            throw new BadRequestException("Email already registered in the system. Email: " + accountRequest.getEmail());
        }
        accountData = accountRepository.findByPhone(accountRequest.getPhone().trim());
        if (accountData != null) {
            throw new BadRequestException("Phone already registered in the system. Phone: " + accountRequest.getPhone());
        }

        Account account = new Account();
        account.setCreateTime(LocalDateTime.now());
        account.setCreatorName("SYSTEM");
        account.setName(accountRequest.getName());
        account.setUsername(accountRequest.getUsername());
        account.setEmail(accountRequest.getEmail());
        account.setPhone(accountRequest.getPhone());
        account.setPassword(accountRequest.getPassword());
        account.setRoles(accountRequest.getRoles());
        account.setDescription(accountRequest.getDescription());

        accountRepository.save(account);

        return account;
    }

    @GetMapping("/all")
    public Page<Account> getAllAccount(@RequestParam(required = false, value = "") String name,
                                       @RequestParam(required = false, value = "") String phone,
                                       @RequestParam(required = false, value = "") String email,
                                       Pageable pageable) {
        return accountRepository.findAll(name, phone, email, pageable);
    }

    @GetMapping("/{accountId}")
    public Account getAccount(@PathVariable String accountId) {
        Optional<Account> accountOptional = accountRepository.findById(accountId);
        if (accountOptional.isEmpty()) {
            throw new NotFoundException("Account data not found on the system. ID: " + accountId);
        }

        return accountOptional.get();
    }

    @PutMapping("/{accountId}")
    public Account editAccount(@PathVariable String accountId, @RequestBody AccountRequest accountRequest) {
        Optional<Account> accountOptional = accountRepository.findById(accountId);
        if (accountOptional.isEmpty()) {
            throw new NotFoundException("Account data not found on the system. ID: " + accountId);
        }

        Account accountData;
        accountData = accountRepository.findByEmail(accountRequest.getEmail().trim().toLowerCase());
        if (accountData != null) {
            if (!accountData.getEmail().equals(accountRequest.getEmail().trim().toLowerCase())) {
                throw new BadRequestException("Email already registered in the system. Email: " + accountRequest.getEmail());
            }
        }
        accountData = accountRepository.findByPhone(accountRequest.getPhone().trim());
        if (accountData != null) {
            if (!accountData.getPhone().equals(accountRequest.getPhone().trim())) {
                throw new BadRequestException("Phone already registered in the system. Phone: " + accountRequest.getPhone());
            }
        }

        Account account = accountOptional.get();
        account.setUpdateTime(LocalDateTime.now());
        account.setUpdaterName("SYSTEM");
        account.setName(accountRequest.getName());
        account.setUsername(accountRequest.getUsername());
        account.setEmail(accountRequest.getEmail());
        account.setPhone(accountRequest.getPhone());
        account.setPassword(accountRequest.getPassword());
        account.setRoles(accountRequest.getRoles());
        account.setDescription(accountRequest.getDescription());

        accountRepository.save(account);

        return account;
    }

    @DeleteMapping("/{accountId}")
    public ObjectNode deleteAccount(@PathVariable String accountId) {
        Optional<Account> accountOptional = accountRepository.findById(accountId);
        if (accountOptional.isEmpty()) {
            throw new NotFoundException("Data akun tidak ditemukan. ID: " + accountId);
        }

        Account account = accountOptional.get();
        account.setDeleteTime(LocalDateTime.now());
        account.setDeleterName("SYSTEM");
        account.setDeleteStatus(true);

        accountRepository.save(account);

        ObjectNode response = objectMapper.createObjectNode();
        response.put("message", "Success delete account. ID: " + accountId);

        return response;
    }
}
