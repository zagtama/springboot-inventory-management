package com.service.inventorymanagement.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.service.inventorymanagement.helpers.exceptions.NotFoundException;
import com.service.inventorymanagement.models.merchant.Merchant;
import com.service.inventorymanagement.models.product.Product;
import com.service.inventorymanagement.models.product.ProductRequest;
import com.service.inventorymanagement.repository.MerchantRepository;
import com.service.inventorymanagement.repository.ProductRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Optional;


@RestController
@RequestMapping("/api/product")
public class ProductController {

    @Autowired
    MerchantRepository merchantRepository;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    ObjectMapper objectMapper;

    @PostMapping("/{merchantId}")
    public Product addProduct(@PathVariable String merchantId, @RequestBody ProductRequest productRequest) {
        Optional<Merchant> merchantOptional = merchantRepository.findById(merchantId);
        if (merchantOptional.isEmpty()) {
            throw new NotFoundException("Merchant data not found. ID: " + merchantId);
        }

        Merchant merchant = merchantOptional.get();
        if (merchant.isDeleteStatus()) {
            throw new NotFoundException("Merchant data not found. ID: " + merchantId);
        }

        Product product = new Product();
        product.setCreateTime(LocalDateTime.now());
        product.setCreatorName("SYSTEM");
        product.setMerchantId(new ObjectId(merchantId));
        product.setName(productRequest.getName());
        product.setDescription(productRequest.getDescription());
        product.setQuantity(productRequest.getQuantity());

        productRepository.insert(product);

        return product;
    }

    @GetMapping("/{merchantId}/all")
    public Page<Product> getAllProduct(@PathVariable String merchantId,
                                       @RequestParam(required = false, value = "") String name,
                                       Pageable pageable) {
        return productRepository.findAllByMerchantId(new ObjectId(merchantId), name, pageable);
    }

    @GetMapping("/{merchantId}/{productId}")
    public Product getProduct(@PathVariable String merchantId, @PathVariable String productId) {
        Optional<Merchant> merchantOptional = merchantRepository.findById(merchantId);
        if (merchantOptional.isEmpty()) {
            throw new NotFoundException("Merchant data not found. ID: " + merchantId);
        }

        Merchant merchant = merchantOptional.get();
        if (merchant.isDeleteStatus()) {
            throw new NotFoundException("Merchant data not found. ID: " + merchantId);
        }

        Optional<Product> productOptional = productRepository.findById(productId);
        if (productOptional.isEmpty()) {
            throw new NotFoundException("Product data not found. ID: " + productId);
        }

        Product product = productOptional.get();
        if (product.isDeleteStatus()) {
            throw new NotFoundException("Product data not found. ID: " + productId);
        }

        if (!product.getMerchantId().toHexString().equals(merchantId)) {
            throw new NotFoundException("Product data not found. ID: " + productId);
        }

        return product;
    }

    @PutMapping("/{merchantId}/{productId}")
    public Product editProduct(@PathVariable String merchantId,
                               @PathVariable String productId,
                               @RequestBody ProductRequest productRequest) {
        Optional<Merchant> merchantOptional = merchantRepository.findById(merchantId);
        if (merchantOptional.isEmpty()) {
            throw new NotFoundException("Merchant data not found. ID: " + merchantId);
        }

        Merchant merchant = merchantOptional.get();
        if (merchant.isDeleteStatus()) {
            throw new NotFoundException("Merchant data not found. ID: " + merchantId);
        }

        Optional<Product> productOptional = productRepository.findById(productId);
        if (productOptional.isEmpty()) {
            throw new NotFoundException("Product data not found. ID: " + productId);
        }

        Product product = productOptional.get();
        if (product.isDeleteStatus()) {
            throw new NotFoundException("Product data not found. ID: " + productId);
        }

        if (!product.getMerchantId().toHexString().equals(merchantId)) {
            throw new NotFoundException("Product data not found. ID: " + productId);
        }

        product.setUpdateTime(LocalDateTime.now());
        product.setUpdaterName("SYSTEM");
        product.setName(productRequest.getName());
        product.setDescription(productRequest.getDescription());
        product.setQuantity(productRequest.getQuantity());

        productRepository.save(product);

        return product;
    }

    @DeleteMapping("/{merchantId}/{productId}")
    public ObjectNode deleteProduct(@PathVariable String merchantId, @PathVariable String productId) {
        Optional<Merchant> merchantOptional = merchantRepository.findById(merchantId);
        if (merchantOptional.isEmpty()) {
            throw new NotFoundException("Merchant data not found. ID: " + merchantId);
        }

        Merchant merchant = merchantOptional.get();
        if (merchant.isDeleteStatus()) {
            throw new NotFoundException("Merchant data not found. ID: " + merchantId);
        }

        Optional<Product> productOptional = productRepository.findById(productId);
        if (productOptional.isEmpty()) {
            throw new NotFoundException("Product data not found. ID: " + productId);
        }

        Product product = productOptional.get();
        if (product.isDeleteStatus()) {
            throw new NotFoundException("Product data not found. ID: " + productId);
        }

        if (!product.getMerchantId().toHexString().equals(merchantId)) {
            throw new NotFoundException("Product data not found. ID: " + productId);
        }

        product.setDeleteTime(LocalDateTime.now());
        product.setDeleterName("SYSTEM");
        product.setDeleteStatus(true);

        productRepository.save(product);

        ObjectNode response = objectMapper.createObjectNode();
        response.put("message", "Success delete product. ID: " + merchantId);
        return response;
    }
}
