package com.service.inventorymanagement.repository;

import com.service.inventorymanagement.models.product.Product;
import com.service.inventorymanagement.services.ProductService;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends MongoRepository<Product, String>, ProductService {

}
