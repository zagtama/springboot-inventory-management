package com.service.inventorymanagement.repository;

import com.service.inventorymanagement.models.account.Account;
import com.service.inventorymanagement.services.AccountService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends JpaRepository<Account, String>, AccountService {

    Account findByUsername(String username);

    Boolean existsByUsername(String username);

    Account findByEmail(String email);

    Boolean existsByEmail(String email);

    Account findByPhone(String phone);

    Boolean existsByPhone(String phone);

    @Query(
            value = "SELECT * FROM accounts WHERE delete_status = false " +
                    "AND CASE WHEN char_length( :name ) >= 3 then name LIKE %:name% ELSE 1=1 END " +
                    "AND CASE WHEN char_length( :email ) >= 3 then email LIKE %:email% ELSE 1=1 END " +
                    "AND CASE WHEN char_length( :phone ) >= 3 then phone LIKE %:phone% ELSE 1=1 END",
            countQuery = "SELECT COUNT(*) FROM accounts WHERE delete_status = false " +
                    "AND CASE WHEN char_length( :name ) >= 3 then name LIKE %:name% ELSE 1=1 END " +
                    "AND CASE WHEN char_length( :email ) >= 3 then email LIKE %:email% ELSE 1=1 END " +
                    "AND CASE WHEN char_length( :phone ) >= 3 then phone LIKE %:phone% ELSE 1=1 END",
            nativeQuery = true)
    Page<Account> findAll(String name, String email, String phone, Pageable pageable);
}
