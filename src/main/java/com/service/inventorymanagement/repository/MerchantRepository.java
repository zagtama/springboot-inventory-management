package com.service.inventorymanagement.repository;

import com.service.inventorymanagement.models.merchant.Merchant;
import com.service.inventorymanagement.services.MerchantService;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MerchantRepository extends MongoRepository<Merchant, String> , MerchantService {

    public Merchant findByEmail(String email);

    public Merchant findByPhone(String phone);
}
