package com.service.inventorymanagement.models.account;

import com.service.inventorymanagement.enums.UserRoles;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AccountRequest {

    @NotNull(message = "Name is mandatory field")
    @NotBlank(message = "Invalid name value")
    private String name;

    @NotNull(message = "Name is mandatory field")
    @NotBlank(message = "Invalid name value")
    private String username;

    @NotNull(message = "Email is mandatory field")
    @NotBlank(message = "Invalid email value")
    @Email(message = "Invalid email value")
    private String email;

    @NotNull(message = "Phone is mandatory field")
    @NotBlank(message = "Invalid phone value")
    private String phone;

    private String password;

    private UserRoles roles = UserRoles.ROLE_USER;

    private String description;
}
