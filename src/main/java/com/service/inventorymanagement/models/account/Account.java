package com.service.inventorymanagement.models.account;

import com.service.inventorymanagement.enums.UserRoles;
import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDateTime;

@Setter
@Getter
@ToString
@Entity
@Table(
        name = "accounts",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "username"),
                @UniqueConstraint(columnNames = "email"),
                @UniqueConstraint(columnNames = "phone")})
public class Account implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID, generator = "uuid1")
    private String id;

    private LocalDateTime createTime;

    private String creatorName;

    private LocalDateTime updateTime;

    private String updaterName;

    private LocalDateTime deleteTime;

    private String deleterName;

    private boolean deleteStatus = false;

    @NotBlank
    @NotNull
    @Size(max = 30)
    private String name;

    @NotBlank
    @NotNull
    @Size(max = 10)
    private String username;

    @Size(max = 50)
    @Email
    private String email;

    @Size(max = 20)
    private String phone;

    private String password;

    @Enumerated(EnumType.STRING)
    private UserRoles roles = UserRoles.ROLE_USER;

    private String description;

    public void setName(String name) {
        this.name = name.trim().toUpperCase();
    }

    public void setUsername(String username) {
        this.username = username.trim().toLowerCase();
    }

    public void setEmail(String email) {
        this.email = email.trim().toLowerCase();
    }

    public void setPhone(String phone) {
        this.phone = phone.trim();
    }

    public void setDescription(String description) {
        this.description = description.trim();
    }
}
