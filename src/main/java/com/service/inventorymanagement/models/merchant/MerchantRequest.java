package com.service.inventorymanagement.models.merchant;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MerchantRequest {
    @NotNull(message = "Name is mandatory field")
    @NotBlank(message = "Invalid name value")
    private String name;

    @NotNull(message = "Email is mandatory field")
    @NotBlank(message = "Invalid email value")
    @Email(message = "Invalid email value")
    private String email;

    @NotNull(message = "Phone is mandatory field")
    @NotBlank(message = "Invalid phone value")
    private String phone;

    private String description;
}
