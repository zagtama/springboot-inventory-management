package com.service.inventorymanagement.models.merchant;

import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.time.LocalDateTime;

@Setter
@Getter
@Document(collection = "merchant")
public class Merchant implements Serializable {
    @Id
    private ObjectId id;

    private LocalDateTime createTime;

    private String creatorName;

    private LocalDateTime updateTime;

    private String updaterName;

    private LocalDateTime deleteTime;

    private String deleterName;

    private boolean deleteStatus = false;

    private String name;

    private String email;

    private String phone;

    private String description;

    public String getId() {
        return id.toHexString();
    }

    public void setName(String name) {
        this.name = name.strip().toUpperCase();
    }

    public void setEmail(String email) {
        this.email = email.trim().toLowerCase();
    }

    public void setPhone(String phone) {
        this.phone = phone.trim();
    }

    public void setDescription(String description) {
        this.description = description.trim();
    }
}
