package com.service.inventorymanagement.models.product;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ProductRequest {
    private String name;
    private String description;
    private int quantity = 0;
}
