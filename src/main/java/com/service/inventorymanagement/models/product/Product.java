package com.service.inventorymanagement.models.product;

import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.time.LocalDateTime;

@Setter
@Getter
public class Product implements Serializable {
    @Id
    private ObjectId id;

    private LocalDateTime createTime;

    private String creatorName;

    private LocalDateTime updateTime;

    private String updaterName;

    private LocalDateTime deleteTime;

    private String deleterName;

    private boolean deleteStatus = false;

    private ObjectId merchantId;

    private String name;

    private String description;

    private int quantity = 0;

    public void setName(String name) {
        this.name = name.trim().toUpperCase();
    }

    public void setDescription(String description) {
        this.description = description.trim();
    }
}
